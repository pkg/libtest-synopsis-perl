libtest-synopsis-perl (0.17-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 17:10:01 +0000

libtest-synopsis-perl (0.17-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.4.1, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.17.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Dec 2021 17:14:25 +0100

libtest-synopsis-perl (0.16-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 01:20:41 +0000

libtest-synopsis-perl (0.16-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.16.
  * Update years of upstream copyright, and drop Comment.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Set upstream metadata fields: Repository-Browse.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Sep 2019 15:24:54 +0200

libtest-synopsis-perl (0.15-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:46:19 +0000

libtest-synopsis-perl (0.15-1) unstable; urgency=medium

  * Import upstream version 0.15

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 02 Mar 2016 20:40:10 +0100

libtest-synopsis-perl (0.14-1) unstable; urgency=medium

  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Import upstream version 0.14
  * Declare compliance with Debian policy 3.9.7

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 17 Feb 2016 17:11:53 +0100

libtest-synopsis-perl (0.13-1) unstable; urgency=medium

  * Import upstream version 0.13
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 05 Jan 2016 20:40:49 +0100

libtest-synopsis-perl (0.12-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.12
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging files
  * Bump Debhelper compat level to 9

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 28 Dec 2015 14:22:55 +0100

libtest-synopsis-perl (0.11-2) unstable; urgency=medium

  * Team upload.
  * Fix an autopkgtest failure by skipping t/06-for.t.

 -- Niko Tyni <ntyni@debian.org>  Sat, 11 Oct 2014 14:25:07 +0300

libtest-synopsis-perl (0.11-1) unstable; urgency=medium

  * Update Vcs-Browser URL to cgit web frontend
  * Add debian/upstream/metadata
  * Imported upstream version 0.11
  * Update copyright years for debian/* packaging
  * Mark package as autopkgtest-able
  * Declare compliance with Debian Policy 3.9.6

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 06 Oct 2014 17:49:31 +0200

libtest-synopsis-perl (0.10-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Update my email address.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * Update debian/repack.stub.
  * Strip trailing slash from metacpan URLs.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.10
  * Bump year of upstream copyright
  * Drop copyright paragraph for M::I, which is no longer included
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux, directly linking to GPL-1)
  * Bump dh compatibility to level 8 (no changes necessary)
  * No longer repack source tarball, inc/ was dropped
  * Switch to source format 3.0 (quilt)
  * Declare compliance with Debian Policy 3.9.5
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 06 Mar 2014 00:02:03 +0100

libtest-synopsis-perl (0.06+ds-1) unstable; urgency=low

  * Initial Release (Closes: #562873)

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Wed, 30 Dec 2009 17:45:23 +0100
